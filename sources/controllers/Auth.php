<?php

namespace app\controllers;

use app\classes\Request;
use app\classes\Template;
use app\classes\Token;
use app\models\User;

class Auth extends BaseController
{
    public function actionLogin(Request $request): void
    {
        $tpl = new Template();

        if ($request->isPost()) {
            $login = (string)$request->getParam('login');
            $pass = (string)$request->getParam('password');
            $user = User::findWhere(['login' => $login, 'pass' => md5($pass)], 1)[0] ?? null;
            if ($user) {
                $_SESSION['userId'] = $user->id;
                $_SESSION['login'] = $login;
                $_SESSION['token'] = Token::get($login);

                $this->redirect('site');
            } else {
                $tpl->set('error', 'Не верный логин или пароль');
            }
        }

        $tpl->set('title', 'Авторизация');
        $tpl->display('auth/login');
    }

    public function actionLogout(): void
    {
        unset($_SESSION['userId']);
        unset($_SESSION['login']);
        unset($_SESSION['token']);
        session_destroy();

        $this->redirect('site');
    }
}
