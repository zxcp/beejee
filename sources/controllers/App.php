<?php

namespace app\controllers;

use app\classes\Request;
use app\classes\Template;

class App
{
    const CONTROLLERS_PATH = 'app\\controllers\\';

    protected static array $errors = [];

    /**
     * Выполняет метод контроллера указанный в $request
     *
     * @param Request $request
     * @return bool
     */
    public static function run(Request $request): bool
    {
        @list($controllerName, $methodName) = explode('/', $request->getRoute());

        $controllerName = self::CONTROLLERS_PATH . ucfirst($controllerName ?: 'site');
        if (class_exists($controllerName)) {
            $controller = new $controllerName();
            $methodName = 'action' . ucfirst($methodName ?? 'index');
            if (method_exists($controller, $methodName)) {
                $controller->$methodName($request);

                return true;
            } else {
                static::$errors[] = 'Метод ' . $methodName .' не найден.';
            }
        } else {
            static::$errors[] = 'Контроллер ' . $controllerName . ' не найден.';
        }

        $tpl = new Template();
        $tpl->display('404', Template::HTTP_NOT_FOUND);

        return false;
    }

    public static function getErrors(): array
    {
        return static::$errors;
    }
}
