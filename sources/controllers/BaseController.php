<?php

namespace app\controllers;

class BaseController
{
    protected function redirect($url): void
    {
        header('Location: //' . $_SERVER['HTTP_HOST'] . '/' . $url, true, 301);
        exit;
    }

    protected function back(): void
    {
        if ($_SERVER['HTTP_REFERER']) {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
        exit;
    }
}
