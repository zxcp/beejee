<?php

namespace app\controllers;

use app\classes\Paginator;
use app\classes\Request;
use app\classes\TaskValidator;
use app\classes\Template;
use app\classes\Token;
use app\models\Task;
use app\models\User;

class Site extends BaseController
{
    public function actionIndex(Request $request): void
    {
        $page = (int)$request->getParam('page', 1);
        $sort = $this->validateSort($request->getParam('sort', []));

        $tpl = new Template();
        $tpl->set('paginator', new Paginator(Task::class, $page, $sort));
        $tpl->set('page', $page);
        $tpl->set('user', $this->getUser());
        $tpl->set('order', $this->getOrder($sort));
        $tpl->set('currentUrl', '/site' . ($page ? "?page=$page" : ''));
        $tpl->set('title', 'Задачи');
        $tpl->display('index');
    }

    public function actionCreate(Request $request): void
    {
        $tpl = new Template();
        if ($request->isPost()) {
            $task = new Task();
            $task->username = (string)$request->getParam('username');
            $task->email = (string)$request->getParam('email');
            $task->text = (string)$request->getParam('text');
            $task->status = Task::STATUS_NEW;

            $taskValidator = new TaskValidator();
            $taskValidator->validate($request->getParams());
            if (!$taskValidator->isValid()) {
                $tpl->set('error', implode('', $taskValidator->getErrors()));
            } elseif ($task->save()) {
                $tpl->set('message', 'Данные успешно сохранены');
                $tpl->set('header', 'Создание задачи');
                $tpl->set('title', 'Статус операции');
                $tpl->display('success');
            } else {
                $tpl->set('error', 'Ошибка сохранения');
            }
        }

        $tpl->set('title', 'Создание задачи');
        $tpl->display('create');
    }

    public function actionEdit(Request $request): void
    {
        $backUrl = $this->getBackUrl((string)$request->getParam('backUrl'));
        if (!$this->getUser()) {
            $this->redirect($backUrl);
        }

        $task = Task::findOne((int)$request->getParam('id'));
        if (!$task) {
            $this->show404('Задача не найдена');
        }

        $tpl = new Template();
        if ($request->isPost()) {
            $taskValidator = new TaskValidator();
            $taskValidator->validate($request->getParams());
            if ($taskValidator->isValid()) {
                $task->username = (string)$request->getParam('username');
                $task->email = (string)$request->getParam('email');
                $task->setText((string)$request->getParam('text'));
                $task->save();

                $this->redirect($backUrl);
            } else {
                $tpl->set('error', implode('', $taskValidator->getErrors()));
            }
        }

        $tpl->set('task', $task);
        $tpl->set('title', 'Редактирование');
        $tpl->set('backUrl', $_SERVER['HTTP_REFERER']);
        $tpl->display('edit');
    }

    public function actionDone(Request $request): void
    {
        if ($this->getUser()) {
            $task = Task::findOne((int)$request->getParam('id'));
            $task->setStatusDone();
            $task->save();
        }

        $this->back();
    }

    private function validateSort(array $sort): array
    {
        if ($sort) {
            $sortField = array_key_first($sort);
            if (in_array($sortField, ['username', 'email', 'status']) && in_array($sort[$sortField], ['ASC', 'DESC'])) {
                return $sort;
            }
        }

        return [];
    }

    private function getUser(): ?User
    {
        if (Token::check($_SESSION['login'] ?? '', $_SESSION['token'] ?? '')) {
            return User::findOne($_SESSION['userId']);
        }

        return null;
    }

    private function show404(string $title): void
    {
        $tpl = new Template();
        $tpl->set('title', $title);
        $tpl->display('404', Template::HTTP_NOT_FOUND);
    }

    private function getBackUrl(string $savedUrl): string
    {
        if ($savedUrl && filter_var($savedUrl, FILTER_VALIDATE_URL)) {
            $parsedUrl = parse_url($savedUrl);

            return substr($parsedUrl['path'], 1) . '?' . $parsedUrl['query'];
        }

        return 'site';
    }

    private function getOrder(array $sort): string
    {
        $sortValues = array_values($sort);
        if (isset($sortValues[0]) && $sortValues[0] === 'DESC') {
            return 'ASC';
        }

        return 'DESC';
    }
}
