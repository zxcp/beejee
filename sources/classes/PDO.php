<?php

namespace app\classes;

use PDOException;

final class PDO
{
    /** @var \PDO|null */
    protected static ?\PDO $pdo = null;

    protected array $pdo_options = [
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES => false,
    ];

    protected function __construct()
    {
        $host = Env::get('host');
        $db = Env::get('db');
        $user = Env::get('user');
        $password = Env::get('password');

        try {
            self::$pdo = new \PDO(
                'mysql:host=' . $host . ';dbname=' . $db . ';charset=utf8',
                $user,
                $password,
                $this->pdo_options
            );
        } catch (PDOException $e) {
            die('Ошибка подключения к БД: ' . $e->getMessage());
        }
    }

    /** @return \PDO */
    public static function getInstance(): \PDO
    {
        if (!self::$pdo) {
            new self();
        }

        return self::$pdo;
    }
}
