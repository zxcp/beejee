<?php

namespace app\classes;

class Request
{
    protected string $route;
    protected array $params;
    protected string $method;

    /**
     * Получение данных запроса
     *
     * @return self
     */
    public static function get(): self
    {
        $request = new self();
        $explodedUri = explode('?', $_SERVER['REQUEST_URI']);
        $urn = array_shift($explodedUri);
        $request->route = trim($urn, '/');
        $request->params = $_REQUEST;
        $request->method = $_SERVER['REQUEST_METHOD'];

        return $request;
    }

    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * Получение параметра запроса по ключу
     *
     * @param string $key
     * @param mixed $defaultValue
     * @return mixed|null
     */
    public function getParam(string $key = '', $defaultValue = null)
    {
        return $this->params[$key] ?? $defaultValue;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function isPost(): bool
    {
        return $this->method === 'POST';
    }
}
