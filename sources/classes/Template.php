<?php

namespace app\classes;

class Template
{
    const EXT = '.tpl';
    const TPL_DIR = '../templates'; // Директория с tpl-файлами
    const COMMON_DIR = self::TPL_DIR . '/common'; // Директория с шаблонами страниц
    const HTTP_OK = 200;
    const HTTP_NOT_FOUND = 404;

    private array $data = []; // Данные для передачи в шаблон

    /**
     * Метод для добавления новых значений для передачи в шаблон
     *
     * @param string $name
     * @param $value
     */
    public function set(string $name, $value): void
    {
        $this->data[$name] = $value;
    }

    /**
     * При обращении, например, к $this->title будет выводиться $this->data["title"]
     *
     * @param string $name
     * @return mixed|string
     */
    public function __get(string $name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }

        return '';
    }

    /**
     * Вывод tpl-файла, в который подставляются все данные для вывода
     *
     * @param string $template
     * @param int $httpCode Возвращаемый код
     */
    public function display(string $template, int $httpCode = self::HTTP_OK): void
    {
        $template = self::TPL_DIR . '/' . $template . self::EXT;
        if (file_exists($template)) {
            header('HTTP/1.1 ' . $httpCode);
            ob_start();
            include self::COMMON_DIR . '/header' . self::EXT;
            include ($template);
            include self::COMMON_DIR . '/footer' . self::EXT;
            echo ob_get_clean();
        } else {
            header('HTTP/1.1 ' . self::HTTP_NOT_FOUND);
            echo 'Не найден файл шаблона: ' . $template;
        }

        exit;
    }
}
