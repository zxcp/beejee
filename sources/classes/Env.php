<?php

namespace app\classes;

class Env
{
    const FILE_NAME = '../.env';
    private static ?array $envData = null;

    /**
     * Возвращает переменные окружения. Если указан ключ, вернёт значение переменной
     *
     * @param string $key
     * @return array|string
     */
    public static function get($key = '')
    {
        if (self::$envData === null) {
            foreach (self::getEnv() as $string) {
                if ($string[0] == '#') continue;

                $param = explode('=', $string);
                self::$envData[$param[0]] = $param[1];
            }
        }

        if ($key) {
            if (isset(self::$envData[$key])) {
                return self::$envData[$key];
            } else {
                return '';
            }
        }

        return self::$envData;
    }

    /**
     * Получение массива строк из файла .env
     *
     * @return array
     */
    private static function getEnv(): array
    {
        if (!file_exists(self::FILE_NAME)) {
            return [];
        }

        return file(self::FILE_NAME, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    }
}
