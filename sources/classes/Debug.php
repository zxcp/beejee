<?php

namespace app\classes;

/**
 * Class Debug
 * Класс для разработки
 * @package app\classes
 */
class Debug
{
    /**
     * Функция для отладки приложения
     *
     * @param $data
     * @param bool $exit
     */
    public static function dd($data, $exit = true): void
    {
        switch (true) {
            case is_object($data):
            case is_array($data):
                echo '<pre>';
                print_r($data);
                echo '</pre>';
                break;
            case is_bool($data):
                $prefix = '(bool) ';
                echo $data ? $prefix . 'true' : $prefix . 'false';
                break;
            default: echo $data . '<br>';
        }

        if ($exit) exit;
    }

    /** Включает отображение ошибок */
    public static function enableErrors()
    {
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
    }
}
