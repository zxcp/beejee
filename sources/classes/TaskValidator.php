<?php

namespace app\classes;

class TaskValidator
{
    private array $errors = [];

    public function validate(array $data): void
    {
        if (empty($data['username'])) {
            $this->registerError('Укажите имя пользователя');
        }

        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $this->registerError('Укажите корректный email');
        }

        if (empty($data['text'])) {
            $this->registerError('Укажите текст задачи');
        }
    }

    public function isValid(): bool
    {
        return $this->errors === [];
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    private function registerError(string $error): void
    {
        $this->errors[] = $error . "<br>";
    }
}
