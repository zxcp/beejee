<?php

namespace app\classes;

class Token
{
    const SECRET = '2050';
    const DATETIME_FORMAT_RU = 'd.m.Y H:i:s';

    public static function get(string $login): string
    {
        $date = new \DateTime();
        $date->setTime(0, 0, 0);// Токен действителен на текущие сутки;
        $stringForHash = $date->format(self::DATETIME_FORMAT_RU) . self::SECRET . $login;

        return md5($stringForHash);
    }

    public static function check(string $login, string $token): bool
    {
        if ($login && $token) {
            return self::get($login) === $token;
        }

        return false;
    }
}
