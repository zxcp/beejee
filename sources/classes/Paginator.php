<?php

namespace app\classes;

class Paginator
{
    const OBJECTS_ON_PAGE = 3;

    private array $objects;
    private string $class;
    private int $page;
    private array $sort;
    private ?int $pageCount = null;

    public function __construct(string $class, int $page = 1, array $sort = [])
    {
        $this->class = $class;
        $this->page = $page;
        $this->sort = $sort;
        $offset = ($page - 1) * self::OBJECTS_ON_PAGE;
        $this->objects = $this->class::paginate($offset, self::OBJECTS_ON_PAGE, $sort);
    }

    public function objects(): array
    {
        return $this->objects;
    }

    public function pageCount(): int
    {
        if ($this->pageCount === null) {
            $this->pageCount = ceil($this->class::count() / self::OBJECTS_ON_PAGE);
        }

        return $this->pageCount;
    }

    public function nextPage()
    {
        if (($this->page + 1) > $this->pageCount()) {
            return $this->pageCount();
        }

        return $this->page + 1;
    }

    public function getSort(): array
    {
        return $this->sort;
    }
}
