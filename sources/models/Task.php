<?php

namespace app\models;

use app\classes\PDO;

/**
 * Class Task
 * @package app\models
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $text
 * @property int $status
 */
class Task extends Model
{
    const STATUS_NEW = 0;
    const STATUS_DONE = 1;
    const STATUS_EDITED = 2;
    const STATUS_DONE_EDITED = 3;

    const STATUS_TEXTS = [
            self::STATUS_NEW => 'Новое',
            self::STATUS_DONE => 'Выполнено',
            self::STATUS_EDITED => 'Отредактировано администратором',
            self::STATUS_DONE_EDITED => 'Выполнено и отредактировано администратором',
        ];

    protected static function tableName(): string
    {
        return 'tasks';
    }

    public static function paginate(int $offset = 0, int $limit = 0, $sort = []): array
    {
        $stmt = PDO::getInstance()->query('SELECT * FROM ' . static::tableName() . ' '
            . self::getSortSql($sort) . ' LIMIT ' . $limit . ' OFFSET ' . $offset);

        return self::queryAsArray($stmt);
    }

    public function setText(string $newText): void
    {
        if ($this->text !== $newText) {
            $this->setStatusEdited();
        }

        $this->text = $newText;
    }

    public function setStatusDone(): void
    {
        if ($this->status == self::STATUS_NEW) {
            $this->status = self::STATUS_DONE;
        } elseif ($this->status == self::STATUS_EDITED) {
            $this->status = self::STATUS_DONE_EDITED;
        }
    }

    public function isDone(): bool
    {
        return $this->status === self::STATUS_DONE || $this->status === self::STATUS_DONE_EDITED;
    }

    public function getStatusText(): string
    {
        return self::STATUS_TEXTS[$this->status];
    }

    private static function getSortSql($sort = []): string
    {
        if ($sort) {
            $sql = 'ORDER BY ';
            $sortField = array_key_first($sort);
            $sql .= $sortField . ' ' . $sort[$sortField];

            return $sql;
        }

        return '';
    }

    private function setStatusEdited(): void
    {
        if ($this->status == self::STATUS_NEW) {
            $this->status = self::STATUS_EDITED;
        } elseif ($this->status == self::STATUS_DONE) {
            $this->status = self::STATUS_DONE_EDITED;
        }
    }
}
