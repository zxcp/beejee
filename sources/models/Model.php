<?php

namespace app\models;

use app\classes\PDO;

/**
 * Base model class
 * @property int $id
 */
abstract class Model
{
    protected static ?Model $model = null;

    abstract protected static function tableName(): string;

    /**
     * @param int $id
     * @return static
     */
    public static function findOne(int $id): ?Model
    {
        $stmt = PDO::getInstance()->query('SELECT * FROM ' . static::tableName() . ' WHERE id = ' . $id);

        if (!$stmt->rowCount()) {
            return null;
        }

        static::$model = new static();
        foreach ($stmt->fetch() as $key => $value) {
            static::$model->$key = $value;
        }

        return static::$model;
    }

    public static function paginate(int $offset = 0, int $limit = 0): array
    {
        $pdo = PDO::getInstance();
        $stmt = $pdo->query('SELECT * FROM ' . static::tableName() . ' LIMIT ' . $limit . ' OFFSET ' . $offset);

        return self::queryAsArray($stmt);
    }

    /**
     * @param array $conditions [$key => $value]
     * @param int $limit
     * @return static[]
     */
    public static function findWhere(array $conditions, int $limit): array
    {
        $where = [];
        $values = [];
        foreach ($conditions as $key => $value) {
            if (is_array($value)) {
                $marks = [];
                foreach ($value as $item) {
                    $values[] = $item;
                    $marks[] = '?';
                }
                $where[] = $key . ' in (' . implode(', ', $marks) . ')';
            } else {
                $where[] = $key . ' = ?';
                $values[] = $value;
            }
        }

        $query = 'SELECT * FROM ' . static::tableName() . ' WHERE ' . implode(' AND ', $where);
        $query .= $limit ? ' LIMIT ' . $limit : '';
        $stmt = PDO::getInstance()->prepare($query);
        $stmt->execute($values);

        return static::queryAsArray($stmt);
    }

    public function save(): bool
    {
        $fields = $this->getColumnNames();
        if (!isset($this->id) && $fields[0] == 'id') {
            unset($fields[0]);
        }

        $values = [];
        $marks = [];//Знаки вопроса
        $onDuplicate = [];
        foreach ($fields as $field) {
            $values[] = $this->$field ?? null;
            $marks[] = '?';
            $onDuplicate[] = $field . ' = ?';
        }

        if ($fields) {
            $query = 'INSERT INTO ' . static::tableName()
                . ' (' . implode(', ', $fields) . ') '
                . ' VALUES (' . implode(', ', $marks) . ')'
                . ' ON DUPLICATE KEY UPDATE ' . implode(', ', $onDuplicate);
            try {
                $pdo = PDO::getInstance();
                $stmt = $pdo->prepare($query);
                $stmt->execute(array_merge($values, $values));

                $insertId = $pdo->lastInsertId();
                if ($insertId) {
                    $this->id = $insertId;
                }

                return true;
            } catch (\Exception $e) {
                // Здесь могла быть ваша реклама
            }
        }

        return false;
    }

    public static function count()
    {
        $stmt = PDO::getInstance()->query('SELECT 1 FROM ' . static::tableName());

        return $stmt->rowCount();
    }

    protected static function queryAsArray(\PDOStatement $stmt): array
    {
        $result = [];
        if ($stmt->rowCount()) {
            while ($row = $stmt->fetch()) {
                $model = new static();
                foreach ($row as $key => $value) {
                    $model->$key = $value;
                }
                $result[] = $model;
            }
        }

        return $result;
    }

    /**
     * Возвращает имена полей таблицы
     *
     * @return array
     */
    private function getColumnNames(): array
    {
        $stmt = PDO::getInstance()->query('SELECT COLUMN_NAME FROM information_schema.COLUMNS '
            . 'WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = \'' . static::tableName()
            . '\' ORDER BY ORDINAL_POSITION'
        );

        return $stmt->fetchAll(\PDO::FETCH_COLUMN);
    }
}
