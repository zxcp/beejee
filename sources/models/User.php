<?php

namespace app\models;

/**
 * @property int $id
 * @property string $login
 * @property string $pass
 * @property string $name
 * @property string $email
 */
class User extends Model
{
    public static function tableName(): string
    {
        return 'users';
    }
}
