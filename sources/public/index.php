<?php

namespace app\classes;

use app\controllers\App;

require "../vendor/autoload.php";

$request = Request::get();
session_start();
if (!App::run($request)) {
    Debug::dd(App::getErrors());
}
