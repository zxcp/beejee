<? if ($this->error): ?>
    <div class="alert alert-danger mt-4" role="alert">
        <?= $this->error ?>
    </div>
<? endif; ?>