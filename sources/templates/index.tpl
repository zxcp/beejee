<div class="container">
    <header class="pt-3 pb-3 d-flex flex-row-reverse">
        <?php if ($this->user): ?>
            <a href="/auth/logout" class="btn btn-primary">Выход (<?= $this->user->name ?>)</a>
        <?php else: ?>
            <a href="/auth/login" class="btn btn-primary">Вход</a>
        <?php endif; ?>
    </header>
</div>
<div class="container">
    <h2 class="text-center mb-5">Задачи</h2>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col"><a href="<?= $this->currentUrl . '&sort[username]=' . $this->order ?>">Пользователь</a></th>
                <th scope="col"><a href="<?= $this->currentUrl . '&sort[email]=' . $this->order ?>">E-mail</a></th>
                <th scope="col">Текст задачи</th>
                <th scope="col"><a href="<?= $this->currentUrl . '&sort[status]=' . $this->order ?>">Статус</a></th>
                <?php if ($this->user): ?>
                    <th scope="col" class="text-center">Действия</th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($this->paginator->objects() as $task): ?>
            <tr>
                <th scope="row"><?= $task->id ?></th>
                <td><?= htmlspecialchars($task->username) ?></td>
                <td><?= $task->email ?></td>
                <td><?= htmlspecialchars($task->text) ?></td>
                <td><?= $task->getStatusText() ?></td>
                <?php if ($this->user): ?>
                    <td class="tasks-actions">
                        <a href="/site/edit?id=<?= $task->id ?>" class="btn btn-secondary">Редактировать</a>
                        <?php if (!$task->isDone()): ?>
                        <a href="/site/done?id=<?= $task->id ?>" class="btn btn-success">Выполнено</a>
                        <?php endif; ?>
                    </td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <div class="text-center">
        <a href="/site/create" class="btn btn-primary">Создать</a>
    </div>

    <nav aria-label="Navigation">
        <ul class="pagination">
            <li class="page-item<?= $this->page == 1 ? ' disabled' : ''?>">
                <a class="page-link" href="/site?page=<?= ($this->page - 1) ?: 1 ?>">Назад</a>
            </li>
            <?php for ($page = 1; $page <= $this->paginator->pageCount(); $page++): ?>
                <li class="page-item<?= $this->page == $page ? ' active' : ''?>">
                    <?php $key = array_key_first($this->paginator->getSort()) ?>
                    <a class="page-link" href="/site?page=<?= $page ?><?= ($this->paginator->getSort()) ? ('&sort[' . $key . ']=' . $this->paginator->getSort()[$key]) : '' ?>">
                        <?= $page ?>
                    </a>
                </li>
            <?php endfor; ?>
            <li class="page-item<?= $this->page == $this->paginator->pageCount() ? ' disabled' : ''?>">
                <a class="page-link" href="/site?page=<?= $this->paginator->nextPage() ?>">Вперёд</a>
            </li>
        </ul>
    </nav>
</div>