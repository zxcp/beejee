<div class="container mw-400">
    <h2 class="text-center mt-5"><?= $this->header ?></h2>

    <?php include 'includes/success.tpl' ?>

    <div class="text-center">
        <a href="/site" class="btn btn-primary">На главную</a>
    </div>
</div>