<div class="container mw-400">
    <h2 class="text-center mt-5 mb-5">Редактирование</h2>

    <?php include 'includes/error.tpl' ?>

    <form action="/site/edit" method="post">
        <div class="mb-3">
            <label for="username" class="form-label">Пользователь</label>
            <input type="text" class="form-control" name="username" id="username" value="<?= $this->task->username ?>">
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">E-mail</label>
            <input type="text" class="form-control" name="email" id="email" value="<?= $this->task->email ?>">
        </div>
        <div class="mb-3">
            <label for="text" class="form-label">Текст задачи</label>
            <input type="text" class="form-control" name="text" id="text" value="<?= $this->task->text ?>">
        </div>
        <input type="hidden" class="form-control" name="id" value="<?= $this->task->id ?>">
        <input type="hidden" class="form-control" name="backUrl" value="<?= $this->backUrl ?>">
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
</div>