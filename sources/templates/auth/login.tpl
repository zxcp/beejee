<div class="container mw-400">
    <h2 class="text-center mt-5">Авторизация</h2>

    <?php include '../templates/includes/error.tpl' ?>

    <form action="/auth/login" method="post">
        <div class="mb-3">
            <label for="login" class="form-label">Логин</label>
            <input type="text" class="form-control" name="login" id="login" required>
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Пароль</label>
            <input type="password" class="form-control" name="password" id="password" required>
        </div>
        <button type="submit" class="btn btn-primary">Вход</button>
    </form>
</div>