<div class="container mw-400">
    <h2 class="text-center mt-5">Создание задачи</h2>

    <?php include 'includes/error.tpl' ?>

    <form action="/site/create" method="post">
        <div class="mb-3">
            <label for="user" class="form-label">Имя пользователя</label>
            <input type="text" class="form-control" name="username" id="user" autocomplete="off">
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">E-mail</label>
            <input type="email" class="form-control" name="email" id="email" autocomplete="off">
        </div>
        <div class="mb-3">
            <label for="text" class="form-label">Текст задачи</label>
            <textarea type="text" class="form-control" name="text" id="text"></textarea>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary">Создать</button>
        </div>
    </form>
</div>